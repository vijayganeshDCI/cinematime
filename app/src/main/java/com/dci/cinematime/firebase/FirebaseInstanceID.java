package com.dci.cinematime.firebase;

import android.content.SharedPreferences;

import com.dci.cinematime.utils.CinemaTimeConstants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseInstanceID extends FirebaseInstanceIdService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        editor.putString(CinemaTimeConstants.FCMTOKEN,refreshedToken).commit();
//        fcmDeviceKey=refreshedToken;
        System.out.println("==========" + refreshedToken + "=======");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences=getSharedPreferences(CinemaTimeConstants.FCMKEYSHAREDPERFRENCES,MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }
}
