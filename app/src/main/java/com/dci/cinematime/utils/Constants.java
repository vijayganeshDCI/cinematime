package com.dci.cinematime.utils;

/**
 * Created by Gowshikan on 7/17/2018.
 */

public class Constants {

    public static String BASE_URL = "http://movietime.dci.in/";
    public static String Userid = "Userid";

    // AppPreference key
    public static String LOGIN_STATUS = "islogin";
    public static String VERSION_CODE = "app_version";
    public static final String LOGINSTATUS="LOGINSTATUS";
    public static final String USERID = "USERID";
    public static final String MOBILENUMBER="MOBILENUMBER";
    public static final String USERNAME="NAME";
    public static final String EMAIL="EMAIL";

}
