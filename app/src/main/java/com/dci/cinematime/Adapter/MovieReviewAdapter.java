package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailReviewItem;

import java.util.List;

/**
 * Created by harini on 7/26/2018.
 */

public class MovieReviewAdapter extends RecyclerView.Adapter<MovieReviewAdapter.MyViewHolder> {
    List<MovieDetailReviewItem> movieDetailReviewList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, description,time;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.reviewer_name);
            description = (TextView) view.findViewById(R.id.description);
            time = (TextView) view.findViewById(R.id.time);

        }
    }

    public MovieReviewAdapter(Context context, List<MovieDetailReviewItem> movieDetailReviewList) {
        this.movieDetailReviewList = movieDetailReviewList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       /* MovieDetail movie = circularsModels.get(position);
        holder.name.setText(movie.getCast().get(position).getCrewLname());
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + movie.getCast().get(position).getCrewImage()).placeholder(R.mipmap.cinema1).
                into(holder.profile);*/
       holder.name.setText(movieDetailReviewList.get(position).getCrew_lname());
       holder.description.setText(movieDetailReviewList.get(position).getReviews());
       holder.time.setText(movieDetailReviewList.get(position).getCreated_at());

    }

    @Override
    public int getItemCount() {
        //  return circularsModels.size();
        return movieDetailReviewList.size();
    }
}

