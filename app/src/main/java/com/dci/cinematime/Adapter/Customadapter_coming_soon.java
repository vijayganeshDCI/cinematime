package com.dci.cinematime.Adapter;

        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.content.SharedPreferences;
        import android.graphics.Color;
        import android.support.annotation.NonNull;
        import android.support.constraint.ConstraintLayout;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.ProgressBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.dci.cinematime.R;
        import com.dci.cinematime.activity.BaseActivity;
        import com.dci.cinematime.app.CinemaTimeApplication;
        import com.dci.cinematime.fragment.ComingSoonFragment;
        import com.dci.cinematime.model.ComingSoon_cl;
        import com.dci.cinematime.model.NotifyMe_cl;
        import com.dci.cinematime.retrofit.ApiClient;
        import com.dci.cinematime.retrofit.CinemaTimeAPI;
        import com.dci.cinematime.utils.Constants;
        import com.dci.cinematime.utils.Util;
        import com.squareup.picasso.Picasso;

        import java.util.ArrayList;
        import java.util.List;

        import javax.inject.Inject;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;

/**
 * Created by Gowshikan on 7/10/2018.
 */

public class Customadapter_coming_soon extends RecyclerView.Adapter<Customadapter_coming_soon.ViewHolder>
{
    List<ComingSoon_cl.Result> coming;
    Context context;
    AddTouchListen addTouchListen;
    public int lastcheckposition = -1;
    BaseActivity baseActivity;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    CinemaTimeAPI cinemaTimeAPI;



    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_label_coming_soon_film_name,text_label_coming_soon_category,
                text_label_coming_soon_notify;
        public ConstraintLayout constraint_notify_me;
        public ImageView image_coming_soon;


        public ViewHolder(View view) {
            super(view);
            text_label_coming_soon_film_name=(TextView)view.findViewById(R.id.text_label_coming_soon_film_name);
            text_label_coming_soon_category=(TextView)view.findViewById(R.id.text_label_coming_soon_category);
            text_label_coming_soon_notify=(TextView)view.findViewById(R.id.text_label_coming_soon_notify);
            image_coming_soon=view.findViewById(R.id.image_coming_soon);
            constraint_notify_me=(ConstraintLayout) view.findViewById(R.id.constraint_notify_me);


        }
    }
    public interface AddTouchListen {
        public void onTouchClick(int position);
       // public void onNotifyTouch(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    public Customadapter_coming_soon(List<ComingSoon_cl.Result> coming, Context context,BaseActivity baseActivity){
        this.coming=coming;
        this.context=context;
        this.baseActivity=baseActivity;
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_childlayout_coming_soon_fragment,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final ComingSoon_cl.Result soon = coming.get(position);
        holder.text_label_coming_soon_film_name.setText(soon.getMovie_original_title());
        holder.text_label_coming_soon_category.setText(soon.getMovie_language());

      //  holder.text_label_coming_soon_notify.setText(String.valueOf(soon.getNotify_status()));
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        +soon.getMovie_backgroundimg()).placeholder(R.drawable.default_movie).into(holder.image_coming_soon);
        /*if (position==lastcheckposition){
            holder.constraint_notify_me.setVisibility(View.VISIBLE);
        }*/

        holder.image_coming_soon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);

            }
        });



        holder.constraint_notify_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isNetworkAvailable()) {
                    //addTouchListen.onNotifyTouch(position);
                    baseActivity.showProgress();

                    cinemaTimeAPI.noti(soon.getId(), sharedPreferences.getInt(Constants.Userid,0)).enqueue(new Callback<NotifyMe_cl>() {
                        //  @SuppressLint("ResourceAsColor")
                        @Override
                        public void onResponse(Call<NotifyMe_cl> call, Response<NotifyMe_cl> response) {
                            baseActivity.hideProgress();
                            //   notify=new ArrayList<>();
                            /*if (response.body().getStatus().equals("Failure")){

                             *//* hideProgress();
                                                 hiderefresh();*//*
                                                 Toast.makeText(getActivity(), "Don't Notify", Toast.LENGTH_SHORT).show();
                                            }*/

                                                 /*hideProgress();
                                                 hiderefresh();*/
                            if (response.body().getResults() != null) {
                                NotifyMe_cl model = response.body();
                                Toast.makeText(context, model.getStatus(), Toast.LENGTH_SHORT).show();

                                if (model.getStatus().equals("Success")) {
                                    //  holder.text_label_coming_soon_notify.setText("tesst");

                                    holder.constraint_notify_me.setBackground(context.getDrawable(R.drawable.background_notifycolour));/*setBackgroundColor(Color.parseColor("#DA3D31"))*/
                                    ;
                                }
                            }
                        }


                        @Override
                        public void onFailure(Call<NotifyMe_cl> call, Throwable t) {
                            baseActivity.hideProgress();
                            Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();

                        }
                    });
              /*lastcheckposition = position;
              notifyDataSetChanged();*/

                }
                else
                {
                    Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show();
                }
            }


        });



    }



    @Override
    public int getItemCount() {
        return coming.size();
    }

}
