package com.dci.cinematime.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.MoviesItem;

import java.util.ArrayList;
import java.util.List;


public class MovieTimeListAdpater extends BaseAdapter   {
    public MovieTimeListAdpater(ArrayList<String> libraryListResultsItems, Context context) {
        this.libraryListResultsItems = libraryListResultsItems;
        this.context = context;



    }

    ArrayList<String> libraryListResultsItems;
    Context context;

    @Override
    public int getCount() {
        return libraryListResultsItems.size();
    }

    @Override
    public String getItem(int position) {
        return libraryListResultsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.custom_spinner_item, null);
        }


        TextView textLibTitle = (TextView) convertView.findViewById(R.id.text_spinner_item_one);
        textLibTitle.setText(libraryListResultsItems.get(position));








        return convertView;
    }



}
