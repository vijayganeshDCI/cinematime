package com.dci.cinematime.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.CategoryItem;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;

import java.util.ArrayList;


public class MovieTiminginTheaterListAdpater extends BaseAdapter   {
    public MovieTiminginTheaterListAdpater(ArrayList<ShowTimeMovieTiminginTheater> moviedetailsCategory, Context context) {
        this.moviedetailsCategory = moviedetailsCategory;
        this.context = context;



    }

    ArrayList<ShowTimeMovieTiminginTheater> moviedetailsCategory;
    Context context;

    @Override
    public int getCount() {
        return moviedetailsCategory.size();
    }

    @Override
    public ShowTimeMovieTiminginTheater getItem(int position) {
        return moviedetailsCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_showtiminglist, null);
        }


        TextView textLibTitle = (TextView) convertView.findViewById(R.id.categoryname);
        textLibTitle.setText(moviedetailsCategory.get(position).getTime());







        return convertView;
    }



}
