package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.MovieDetail;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by harini on 7/25/2018.
 */

public class MovieCastAdapter extends RecyclerView.Adapter<MovieCastAdapter.MyViewHolder> {
    ArrayList<CastItem> moviedetailsCast;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView profile;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.text_label_email_name);
            profile = (ImageView) view.findViewById(R.id.profile);

        }
    }

    public MovieCastAdapter(Context context ,ArrayList<CastItem> moviedetailsCast) {
        this.moviedetailsCast =moviedetailsCast ;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cast_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + moviedetailsCast.get(position).getCrew_image()).placeholder(R.mipmap.cinema1).
                into(holder.profile);
       holder.name.setText(moviedetailsCast.get(position).getCrew_fname());



    }

    @Override
    public int getItemCount() {
       // return circularsModels.size();
        return moviedetailsCast.size();
    }
}

