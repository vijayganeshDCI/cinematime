package com.dci.cinematime.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.util.LogTime;
import com.dci.cinematime.R;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailsTheatresList;

import java.util.Collections;
import java.util.List;

import static com.dci.cinematime.activity.MovieDetailActivity.moviedetailsCategory;

/**
 * Created by harini on 7/26/2018.
 */

public class MovieTheaterAdapter  extends RecyclerView.Adapter<MovieTheaterAdapter.MyViewHolder> {
    List<MovieDetailsTheatresList> movieDetailTheatresList;
    Context context;
    MovieTiminginTheaterListAdpater movieTiminginTheaterListAdpater;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address;
        ImageView profile;
        GridView theater_movie_timing;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.theater_name);
            address = (TextView) view.findViewById(R.id.address);
            profile = (ImageView) view.findViewById(R.id.profile);
            theater_movie_timing=(GridView)view.findViewById(R.id.theater_movie_timing);

        }
    }

    public MovieTheaterAdapter(Context context,   List<MovieDetailsTheatresList> movieDetailTheatresList) {
        this.movieDetailTheatresList = movieDetailTheatresList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_theater_list_item
                        , parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(movieDetailTheatresList.get(position).getName());


        movieTiminginTheaterListAdpater=new MovieTiminginTheaterListAdpater(movieDetailTheatresList.get(position).getMovietimeinTheatres(),context);
        holder.theater_movie_timing.setAdapter(movieTiminginTheaterListAdpater);


    }

    @Override
    public int getItemCount() {
        //  return circularsModels.size();
        return movieDetailTheatresList.size();
    }
}


