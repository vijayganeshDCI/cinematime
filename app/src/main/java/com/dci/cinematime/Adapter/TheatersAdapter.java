package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.TheatersList;

import java.util.List;

/**
 * Created by harini on 7/27/2018.
 */

public class TheatersAdapter extends RecyclerView.Adapter<TheatersAdapter.ViewHolder> {

    private List<TheatersList.ResultsItem> theaterlist;
    private Context context;
    AddTouchListen addTouchListen;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theatername, address, status, percentage, views;
        public ImageView image_now_showing;
        RelativeLayout layout;


        public ViewHolder(View view) {
            super(view);
            theatername = (TextView) view.findViewById(R.id.theater_name);
            address = (TextView) view.findViewById(R.id.theater_area);
           /* status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);*/
            layout = view.findViewById(R.id.layout);

        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public TheatersAdapter(List<TheatersList.ResultsItem> showing, Context context) {
        this.theaterlist = showing;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.theater_item_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TheatersList.ResultsItem now = theaterlist.get(position);
        holder.theatername.setText(now.getTheatrename());
        holder.address.setText(now.getCitylist().getCityname() + ", " + now.getstatee().getStatename() + ", " + now.getCountry().getCountryname());


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return theaterlist.size();
    }


}

