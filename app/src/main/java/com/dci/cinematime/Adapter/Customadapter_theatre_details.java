package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.TheatreDetails_cl;

import java.util.List;

/**
 * Created by Gowshikan on 7/13/2018.
 */

public class Customadapter_theatre_details extends RecyclerView.Adapter<Customadapter_theatre_details.ViewHolder>
{

    private List<TheatreDetails_cl>theatre;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_label_theatre_details_film_name,text_label_theatre_details_category
                ,text_label_theatre_time,text_label_theatre_time_two;
        public ViewHolder(View view) {
            super(view);
            text_label_theatre_details_film_name=(TextView)view.findViewById(R.id.text_label_theatre_details_film_name);
            text_label_theatre_details_category=(TextView)view.findViewById(R.id.text_label_theatre_details_category);
            text_label_theatre_time=(TextView)view.findViewById(R.id.text_label_theatre_time);
            text_label_theatre_time_two=(TextView)view.findViewById(R.id.text_label_theatre_time_two);

        }
    }
    public Customadapter_theatre_details(List<TheatreDetails_cl>theatre,Context context){
        this.theatre=theatre;
        this.context=context;
    }


    @NonNull
    @Override
    public Customadapter_theatre_details.ViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_childlayout_theatre_details,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Customadapter_theatre_details.ViewHolder holder, int position) {
        TheatreDetails_cl details = theatre.get(position);
        holder.text_label_theatre_details_film_name.setText(details.getText_label_theatre_details_film_name());
        holder.text_label_theatre_details_category.setText(details.getText_label_theatre_details_category());
        holder.text_label_theatre_time.setText(details.getText_label_theatre_time());
        holder.text_label_theatre_time_two.setText(details.getText_label_theatre_time_two());


    }

    @Override
    public int getItemCount() {
        return theatre.size();
    }


}
