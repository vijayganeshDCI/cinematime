package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.CrewItem;
import com.dci.cinematime.model.MovieDetail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by harini on 7/25/2018.
 */

public class MovieCrewAdapter extends RecyclerView.Adapter<MovieCrewAdapter.MyViewHolder> {
    ArrayList<CrewItem> moviedetailsCrew;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,role;
        public ImageView profile;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            role = (TextView) view.findViewById(R.id.role);
            profile = (ImageView) view.findViewById(R.id.profile);

        }
    }

    public MovieCrewAdapter(Context context, ArrayList<CrewItem> moviedetailsCrew) {
        this.moviedetailsCrew=moviedetailsCrew;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crew_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       /* MovieDetail movie = circularsModels.get(position);
        holder.name.setText(movie.getCast().get(position).getCrewLname());
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + movie.getCast().get(position).getCrewImage()).placeholder(R.mipmap.cinema1).
                into(holder.profile);*/
       holder.name.setText(moviedetailsCrew.get(position).getName());
       holder.role.setText(moviedetailsCrew.get(position).getRole());

    }

    @Override
    public int getItemCount() {
      //  return circularsModels.size();
        return moviedetailsCrew.size();
    }
}

