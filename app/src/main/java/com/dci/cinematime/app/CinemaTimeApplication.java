package com.dci.cinematime.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;


import com.dci.cinematime.BuildConfig;
import com.dci.cinematime.dagger.AppModule;
import com.dci.cinematime.dagger.ApplicationComponent;
import com.dci.cinematime.dagger.DaggerApplicationComponent;
import com.dci.cinematime.retrofit.RetrofitModule;

import javax.inject.Inject;

public class CinemaTimeApplication extends MultiDexApplication {
    @Inject
    public SharedPreferences mPrefs;
    private static CinemaTimeApplication mInstance;

    public static CinemaTimeApplication getContext() {
        return mInstance;
    }

    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(BuildConfig.CINEMA_TIME_BASE_URL))
                .build();
        mComponent.inject(this);



//        // UNIVERSAL IMAGE LOADER SETUP
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .cacheOnDisk(true).cacheInMemory(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .displayer(new FadeInBitmapDisplayer(300)).build();
//
//        ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(
//                getApplicationContext())
//                .defaultDisplayImageOptions(defaultOptions)
//                .memoryCache(new WeakMemoryCache())
//                .diskCacheSize(100 * 1024 * 1024).build();
//
//        ImageLoader.getInstance().init(imageLoaderConfiguration);


    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static CinemaTimeApplication from(@NonNull Context context) {
        return (CinemaTimeApplication) context.getApplicationContext();
    }

}
