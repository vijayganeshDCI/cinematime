package com.dci.cinematime.dagger;


import android.content.SharedPreferences;


import com.dci.cinematime.Adapter.Customadapter_coming_soon;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.LoginActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.activity.SignupActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.fragment.ComingSoonFragment;
import com.dci.cinematime.fragment.DescriptionFragment;
import com.dci.cinematime.fragment.ExclusiveFragment;
import com.dci.cinematime.fragment.NowShowingFragment;
import com.dci.cinematime.fragment.ReviewFragment;
import com.dci.cinematime.fragment.SearchFragment;
import com.dci.cinematime.fragment.ShowtimeFragment;
import com.dci.cinematime.fragment.TheatresFragment;
import com.dci.cinematime.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    SharedPreferences sharedPreferences();

    Retrofit retrofit();


    void inject(CinemaTimeApplication cinemaTimeApplication);

    void inject(LoginActivity loginActivity);
    void inject(ReviewFragment reviewFragment);

    void inject(Customadapter_coming_soon customadapter_coming_soon);
    void inject(SignupActivity signupActivity);
    void inject(ShowtimeFragment showtimeFragment);
    void inject(NowShowingFragment nowShowingFragment);
    void inject(ComingSoonFragment comingSoonFragment);
    void inject(ExclusiveFragment exclusiveFragment);
    void inject(DescriptionFragment descriptionFragment);
    void inject(SearchFragment searchFragment);
    void inject(HomeFilterPopupActivity homeFilterPopupActivity);
    void inject(TheatresFragment  theatresFragment);
    void inject(MovieDetailActivity movieDetailActivity);
    void inject(TheatreDetailsActivity theatreDetailsActivity);

}
