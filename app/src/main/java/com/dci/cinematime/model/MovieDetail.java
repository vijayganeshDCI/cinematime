package com.dci.cinematime.model;

import android.support.annotation.Nullable;

import java.util.List;

public class MovieDetail{
//	private String movie_category;
//	private String movie_certificate;
//	private String movie_original_title;
//	private String movie_trailer;
	private String movie_vote_average;
//	private String movie_backgroundimg;
	private String movie_description;
//	private String movie_posterimage;
//	private String movie_casts;

	public String getMovie_description() {
		return movie_description;
	}

	public void setMovie_description(String movie_description) {
		this.movie_description = movie_description;
	}

	public String getMovie_language() {
		return movie_language;
	}

	public void setMovie_language(String movie_language) {
		this.movie_language = movie_language;
	}

	private String name;
	private String movie_language;
	private int movie_length;
	private int movie_vote_count;
//	private int id;
//	private String movie_crews;
	@Nullable
	private List<CategoryItem> category;
//	private String movie_tagline;

	public List<CategoryItem> getCategory() {
		return category;
	}

	public void setCategory(List<CategoryItem> category) {
		this.category = category;
	}

	private List<CrewItem> crew;

	public List<CrewItem> getCrew() {
		return crew;
	}

	public void setCrew(List<CrewItem> crew) {
		this.crew = crew;
	}

	private List<CastItem> cast;

	public List<CastItem> getCast() {
		return cast;
	}

	public void setCast(List<CastItem> cast) {
		this.cast = cast;
	}

	public String getMovie_vote_average() {
		return movie_vote_average;
	}

	public void setMovie_vote_average(String movie_vote_average) {
		this.movie_vote_average = movie_vote_average;
	}

	public int getMovie_length() {
		return movie_length;
	}

	public void setMovie_length(int movie_length) {
		this.movie_length = movie_length;
	}

	public int getMovie_vote_count() {
		return movie_vote_count;
	}

	public void setMovie_vote_count(int movie_vote_count) {
		this.movie_vote_count = movie_vote_count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}