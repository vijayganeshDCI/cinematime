package com.dci.cinematime.model;

import java.util.ArrayList;

public class MovieDetailsTheatresList
{
    private String name;
    private String Latitude;
    private String Longitude;
    private String Email;
    private String NoofScreens;
    private ArrayList<ShowTimeMovieTiminginTheater> movietimeinTheatres;
    public MovieDetailsTheatresList(String name, String latitude, String longitude, String email, String noofScreens,ArrayList<ShowTimeMovieTiminginTheater> movietimeinTheatres) {
        this.name = name;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.Email = email;
        this.NoofScreens = noofScreens;
        this.movietimeinTheatres=movietimeinTheatres;
    }

    public ArrayList<ShowTimeMovieTiminginTheater> getMovietimeinTheatres() {
        return movietimeinTheatres;
    }

    public void setMovietimeinTheatres(ArrayList<ShowTimeMovieTiminginTheater> movietimeinTheatres) {
        this.movietimeinTheatres = movietimeinTheatres;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNoofScreens() {
        return NoofScreens;
    }

    public void setNoofScreens(String noofScreens) {
        NoofScreens = noofScreens;
    }
}
