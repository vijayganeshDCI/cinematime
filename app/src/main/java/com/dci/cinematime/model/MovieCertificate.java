package com.dci.cinematime.model;

public class MovieCertificate{
	private String movieCertificate;
	private String updatedAt;
	private String description;
	private String createdAt;
	private int id;
	private int status;

	public void setMovieCertificate(String movieCertificate){
		this.movieCertificate = movieCertificate;
	}

	public String getMovieCertificate(){
		return movieCertificate;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MovieCertificate{" + 
			"movie_certificate = '" + movieCertificate + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",description = '" + description + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
