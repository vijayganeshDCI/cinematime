package com.dci.cinematime.model;

import java.util.List;

public class SearchResults {
	private List<MoviesItem> movies;
	private List<TheatresItem> theatres;

	public List<MoviesItem> getMovies() {
		return movies;
	}

	public void setMovies(List<MoviesItem> movies) {
		this.movies = movies;
	}

	public List<TheatresItem> getTheatres() {
		return theatres;
	}

	public void setTheatres(List<TheatresItem> theatres) {
		this.theatres = theatres;
	}

}