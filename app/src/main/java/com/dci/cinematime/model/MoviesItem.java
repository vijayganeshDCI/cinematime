package com.dci.cinematime.model;

import java.util.List;

public class MoviesItem{

	private String movie_original_title;
	private String name;
	private int id;
	private String crew_name;
	private int category;

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getCrew_name() {
		return crew_name;
	}

	public void setCrew_name(String crew_name) {
		this.crew_name = crew_name;
	}

	public String getMovie_original_title() {
		return movie_original_title;
	}

	public void setMovie_original_title(String movie_original_title) {
		this.movie_original_title = movie_original_title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}