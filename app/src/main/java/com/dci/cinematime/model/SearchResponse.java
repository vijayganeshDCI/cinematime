package com.dci.cinematime.model;

public class SearchResponse{
	private String Status;
	private SearchResults Results;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public SearchResults getResults() {
		return Results;
	}

	public void setResults(SearchResults results) {
		Results = results;
	}
}
