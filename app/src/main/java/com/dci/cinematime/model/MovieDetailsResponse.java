package com.dci.cinematime.model;

public class MovieDetailsResponse {
	private String Status;
	private MovieDetail Results;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public MovieDetail getResults() {
		return Results;
	}

	public void setResults(MovieDetail results) {
		Results = results;
	}
}
