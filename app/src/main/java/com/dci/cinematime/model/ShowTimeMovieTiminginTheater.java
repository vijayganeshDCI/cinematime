package com.dci.cinematime.model;

public class ShowTimeMovieTiminginTheater
{
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ShowTimeMovieTiminginTheater(String time) {
        this.time = time;
    }
}
