package com.dci.cinematime.model;

public class CategoryItem{
	private String categoryName;

	private int id;
	private int categoryTmdbId;
	private int status;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategoryTmdbId() {
		return categoryTmdbId;
	}

	public void setCategoryTmdbId(int categoryTmdbId) {
		this.categoryTmdbId = categoryTmdbId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
