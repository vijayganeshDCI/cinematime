package com.dci.cinematime.model;

/**
 * Created by Gowshikan on 7/17/2018.
 */

public class SignupModel {

    private String Status;
    private Results Results;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public Results getResults() {
        return Results;
    }

    public void setResults(Results results) {
        this.Results = results;
    }
    public class Results {

        private String user_fname;
        private String email;
        private String phone;
        private String password;
        private String device_id;
        private String device_name;
        private String device_imei;
        private String device_os;
        private String app_version;
        private String updated_at;
        private String created_at;
        private Integer id;

        public String getUser_fname() {
            return user_fname;
        }

        public void setUser_fname(String user_fname) {
            this.user_fname = user_fname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getDevice_name() {
            return device_name;
        }

        public void setDevice_name(String device_name) {
            this.device_name = device_name;
        }

        public String getDevice_imei() {
            return device_imei;
        }

        public void setDevice_imei(String device_imei) {
            this.device_imei = device_imei;
        }

        public String getDevice_os() {
            return device_os;
        }

        public void setDevice_os(String device_os) {
            this.device_os = device_os;
        }

        public String getApp_version() {
            return app_version;
        }

        public void setApp_version(String app_version) {
            this.app_version = app_version;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }

}
