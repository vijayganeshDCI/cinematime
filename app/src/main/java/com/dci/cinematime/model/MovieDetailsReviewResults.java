package com.dci.cinematime.model;

import java.util.List;

public class MovieDetailsReviewResults {

	private List<MovieDetailReviewDataItem> data;
	private int current_page;

	public List<MovieDetailReviewDataItem> getData() {
		return data;
	}

	public void setData(List<MovieDetailReviewDataItem> data) {
		this.data = data;
	}

	public int getCurrent_page() {
		return current_page;
	}

	public void setCurrent_page(int current_page) {
		this.current_page = current_page;
	}
}