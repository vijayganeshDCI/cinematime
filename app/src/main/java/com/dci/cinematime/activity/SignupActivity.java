package com.dci.cinematime.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.dagger.AppModule_ProvideDMKApiInterfaceFactory;
import com.dci.cinematime.model.SignupModel;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.retrofit.RetrofitModule;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.dci.cinematime.utils.UtilsDefault;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.cinematime.utils.UtilsDefault.isEmailValid;
import static dagger.internal.InstanceFactory.create;

public class SignupActivity extends BaseActivity {

    TextView text_label_log_in, text_label_sign_skip;
    EditText edit_text_label_your_name, edit_text_label_email, edit_text_label_mobile_number, edit_text_label_password;
    Button button_sign_up;
    ProgressDialog progressDialog;
    String username, emailid, mobilenum, passwrd;
    boolean isrefresh;
    TelephonyManager telephonyManager;
    String device_id;
    String device_name;
    String version_name;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    CinemaTimeAPI cinemaTimeAPI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        device_id = telephonyManager.getDeviceId();
        device_name = Build.MODEL;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version_name = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        initializeView();
    }

    private void initializeView() {
        text_label_log_in = findViewById(R.id.text_label_log_in);
        edit_text_label_your_name = findViewById(R.id.edit_text_label_your_name);
        edit_text_label_email = findViewById(R.id.edit_text_label_email);
        text_label_sign_skip = findViewById(R.id.text_label_sign_skip);
        edit_text_label_mobile_number = findViewById(R.id.edit_text_label_mobile_number);
        edit_text_label_password = findViewById(R.id.edit_text_label_password);
        button_sign_up = findViewById(R.id.button_sign_up);
        initializeListener();
        progressDialog = new ProgressDialog(this);

    }

    private void initializeListener() {
        text_label_log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
        text_label_sign_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 username = edit_text_label_your_name.getText().toString().trim();
                 emailid = edit_text_label_email.getText().toString().trim();
                 mobilenum = edit_text_label_mobile_number.getText().toString().trim();
                 passwrd = edit_text_label_password.getText().toString().trim();

                if (username.equals("")) {
                    Toast.makeText(SignupActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
                } else if (!isEmailValid(emailid)) {
                    Toast.makeText(SignupActivity.this, "Enter vaild email", Toast.LENGTH_SHORT).show();
                } else if (mobilenum.equals("")) {
                    Toast.makeText(SignupActivity.this, "Please enter your mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobilenum.length() != 10) {
                    Toast.makeText(SignupActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (passwrd.equals("")) {
                    Toast.makeText(SignupActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                } else if (passwrd.length() <= 8) {
                    Toast.makeText(SignupActivity.this, "Please enter your correct password", Toast.LENGTH_SHORT).show();
                } else {

                    getData();
                }
            }
        });
    }

    private void getData() {
        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait...");

            cinemaTimeAPI.signup(username, emailid, mobilenum, passwrd,Settings.Secure.ANDROID_ID, device_name, device_id, Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),version_name).enqueue(new Callback<SignupModel>() {
                @Override
                public void onResponse(Call<SignupModel> call, Response<SignupModel> response) {
                    hideProgress();
                    if (response.body() != null) {
                        SignupModel model = response.body();
                       // UtilsDefault.updateSharedPreference(Constants.Userid,String.valueOf(model.getMovieDetailsReviewResults().getId()));
                        if (model.getStatus().equals("Success")) {
                            editor.putInt(Constants.LOGINSTATUS, 1);
                            editor.putInt(Constants.Userid,model.getResults().getId());
                            editor.putString(Constants.USERNAME,model.getResults().getUser_fname());
                            editor.putString(Constants.MOBILENUMBER,model.getResults().getPhone());
                            editor.commit();

                            Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            Toast.makeText(SignupActivity.this, "success", Toast.LENGTH_SHORT).show();
                        }else{
                            // hideProgress();
                            Toast.makeText(SignupActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }

                    }
                }


                @Override
                public void onFailure(Call<SignupModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(SignupActivity.this, "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            hideProgress();
            Toast.makeText(this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }
}




