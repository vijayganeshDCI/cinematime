package com.dci.cinematime.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.CategoryListAdpater;
import com.dci.cinematime.Adapter.TheatersAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.fragment.DescriptionFragment;
import com.dci.cinematime.fragment.ReviewFragment;
import com.dci.cinematime.fragment.ShowtimeFragment;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.CategoryItem;
import com.dci.cinematime.model.CrewItem;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailsResponse;
import com.dci.cinematime.model.MoviesItem;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.ShoppingApplication;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by harini on 7/25/2018.
 */

public class MovieDetailActivity extends BaseActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView playicon;
    RelativeLayout back,calendar_view;
    GridView categoryGridview;
    MovieDetailsResponse movieDetail;
    public static  int movieId;
    TextView moviename;
    TextView percentage;
    TextView movieduration;
    TextView views;
    TextView language;
    ImageView posterImage;
    public static String description="test";
    public static ArrayList<CastItem> moviedetailsCast;
    public static ArrayList<CrewItem> moviedetailsCrew;
    public static ArrayList<CategoryItem> moviedetailsCategory;
    CategoryListAdpater categoryListAdpater;
    String videoID;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        moviename=(TextView)findViewById(R.id.moviename);
        percentage=(TextView)findViewById(R.id.percentage);
        movieduration=(TextView)findViewById(R.id.movieduration);
        posterImage=(ImageView)findViewById(R.id.movie_image);
        views=(TextView)findViewById(R.id.views);
        language=(TextView)findViewById(R.id.language);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        categoryGridview=(GridView)findViewById(R.id.categoryGridview);
        moviedetailsCast=new ArrayList<CastItem>();
        moviedetailsCrew=new ArrayList<CrewItem>();
        moviedetailsCategory=new ArrayList<CategoryItem>();
        playicon = (ImageView) findViewById(R.id.playicon);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        back = findViewById(R.id.back);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            movieId = bundle.getIntExtra("id", 0);

        }
        playicon.setVisibility(View.GONE);
        getMoviedetail();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        playicon.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent i = new Intent(MovieDetailActivity.this, YoutubePlayerActivity.class);
                i.putExtra("videoid", videoID);
                startActivity(i);



            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ShowtimeFragment(), "Show Times");
        adapter.addFragment(new DescriptionFragment(), "Description");
        adapter.addFragment(new ReviewFragment(), "  Review  ");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            title.toLowerCase();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getMoviedetail() {
        if (Util.isNetworkAvailable()) {

            //  swip_lay.setRefreshing(true);
            showProgress();

            cinemaTimeAPI.getmoviesDetail(movieId).enqueue(new Callback<JsonElement>() {
               @Override
               public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                   hideProgress();


                   if (response.body() != null) {
                       try {
                           JSONObject object = new JSONObject(String.valueOf(response.body()));
                           JSONObject jsonArray = object.getJSONObject("Results");
                           for (int i = 0; i < 1; i++) {
                               moviename.setText(jsonArray.getString("name"));
                               percentage.setText(jsonArray.getString("movie_vote_average"));
                               movieduration.setText("Duration: "+jsonArray.getString("movie_length")+" Min");
                               language.setText(jsonArray.getString("movie_language"));
                               description=jsonArray.getString("movie_description");
                                videoID=jsonArray.getString("movie_trailer");
                               if (videoID.length()>0)
                               {
                                   playicon.setVisibility(View.VISIBLE);
                               }
                               Picasso.with(getApplicationContext()).
                                       load(getApplicationContext().getString(R.string.coming_soon_image_base_url)
                                               + jsonArray.getString("movie_backgroundimg")).placeholder(R.drawable.default_movie).
                                       into(posterImage);
                               JSONArray castarray = jsonArray.getJSONArray("cast");
                               for (int j = 0; j < castarray.length(); j++) {
                                   JSONObject moviesListcastObject = castarray.getJSONObject(j);
                                   CastItem castItem = new CastItem();
                                   castItem.setId(moviesListcastObject.getInt("id"));
                                  castItem.setCrew_fname(moviesListcastObject.getString("crew_fname"));
                                   castItem.setCrew_lname(moviesListcastObject.getString("crew_lname"));
                                   moviedetailsCast.add(castItem);




                               }
                               JSONArray crewarray = jsonArray.getJSONArray("crew");
                               for (int j = 0; j < crewarray.length(); j++) {
                                   JSONObject moviesListCrewObject = crewarray.getJSONObject(j);
                                   CrewItem crewItem = new CrewItem();
                                   crewItem.setId(moviesListCrewObject.getInt("id"));
                                   crewItem.setName(moviesListCrewObject.getString("name"));
                                   crewItem.setRole(moviesListCrewObject.getString("role"));
                                   moviedetailsCrew.add(crewItem);




                               }
                               Object searchresult;
                               searchresult = jsonArray.get("category");
                               boolean check = false;
                               if (searchresult instanceof JSONArray) {
                                   // It's an array
                                   check=true;
                               }
                               else if (searchresult instanceof JSONObject) {
                                   // It's an object
                                   check=false;
                               }
                               if (check)
                               {
                                   JSONArray categoryarray = jsonArray.getJSONArray("category");
                                   for (int j = 0; j < categoryarray.length(); j++) {
                                       JSONObject moviesListcategoryObject = categoryarray.getJSONObject(j);
                                       CategoryItem categoryItem=new CategoryItem();
                                       categoryItem.setId(moviesListcategoryObject.getInt("id"));
                                       categoryItem.setStatus(moviesListcategoryObject.getInt("status"));
                                       categoryItem.setCategoryName(moviesListcategoryObject.getString("category_name"));
                                       categoryItem.setCategoryTmdbId(moviesListcategoryObject.getInt("category_tmdb_id"));
                                       moviedetailsCategory.add(categoryItem);

                                   }
                                   categoryListAdpater=new CategoryListAdpater(moviedetailsCategory,getApplicationContext());
                                   categoryGridview.setAdapter(categoryListAdpater);
                               }
                               else
                               {
                                   Log.d("Vignesh", "category");
                               }



                           }
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
//                       moviename.setText(movieDetail.getMovieDetailsReviewResults().getName());
//                        percentage.setText(movieDetail.getMovieDetailsReviewResults().getMovie_vote_average());
//                        movieduration.setText("Duration: "+movieDetail.getMovieDetailsReviewResults().getMovie_length()+" Min");
//                        language.setText(movieDetail.getMovieDetailsReviewResults().getMovie_language());
//                        description=movieDetail.getMovieDetailsReviewResults().getMovie_description();
//                        for (int i=0;i<movieDetail.getMovieDetailsReviewResults().getCast().size();i++)
//                        {
//                            moviedetailsCast.add(new CastItem(movieDetail.getMovieDetailsReviewResults().getCast().get(i).getId(),movieDetail.getMovieDetailsReviewResults().getCast().get(i).getCrew_fname(),
//                                    movieDetail.getMovieDetailsReviewResults().getCast().get(i).getCrew_lname(),movieDetail.getMovieDetailsReviewResults().getCast().get(i).getCrew_image()));
//                        }
//                       for (int i=0;i<movieDetail.getMovieDetailsReviewResults().getCrew().size();i++)
//                       {
//                           moviedetailsCrew.add(new CrewItem(movieDetail.getMovieDetailsReviewResults().getCrew().get(i).getRole(),movieDetail.getMovieDetailsReviewResults().getCrew().get(i).getName(),
//                                   movieDetail.getMovieDetailsReviewResults().getCrew().get(i).getId()));
//                       }
                        setupViewPager(viewPager);
                   }
                   else
                   {
                       Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<JsonElement> call, Throwable t) {
                hideProgress();
                   Toast.makeText(getApplicationContext(), "onFailure", Toast.LENGTH_SHORT).show();
               }
           });
        } else {
            Toast.makeText(this, "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }


}
