package com.dci.cinematime.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.dagger.ApplicationComponent;
import com.dci.cinematime.model.LoginModel;
import com.dci.cinematime.model.SignupModel;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.dci.cinematime.utils.UtilsDefault;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    /*@BindView(R.id.image_login_pop_corn)
    ImageView imageLoginPopCorn;
    @BindView(R.id.text_label_login_skip)
    TextView textLabelLoginSkip;
    @BindView(R.id.text_label_glad_to_see_you)
    TextView textLabelGladToSeeYou;
    @BindView(R.id.edit_text_label_login_user_name)
    EditText editTextLabelLoginUserName;
    @BindView(R.id.edit_text_label_login_password)
    EditText editTextLabelLoginPassword;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.text_label_forgot_user_name_password)
    TextView textLabelForgotUserNamePassword;
    @BindView(R.id.edit_text_login_or_connect_with)
    TextView editTextLoginOrConnectWith;
    @BindView(R.id.button_login_facebook)
    Button buttonLoginFacebook;
    @BindView(R.id.button_login_gmail)
    Button buttonLoginGmail;
    @BindView(R.id.text_label_login_dont_have_an_account)
    TextView textLabelLoginDontHaveAnAccount;
    @BindView(R.id.text_label_sign_up)*/
    TextView textLabelSignUp, textLabelLoginSkip;
    TextView text_label_forgot_user_name_password, text_label_sign_up;
    AlertDialog alertDialog;
    Button buttonLogin, buttonLoginFacebook;
    EditText editTextLabelLoginUserName, editTextLabelLoginPassword;
    String username_login, username_paswd;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    CinemaTimeAPI cinemaTimeAPI;
    TelephonyManager telephonyManager;
    String device_id;
    String device_name;
    String version_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        editTextLabelLoginUserName = findViewById(R.id.edit_text_label_login_user_name);
        editTextLabelLoginPassword = findViewById(R.id.edit_text_label_login_password);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        device_id = telephonyManager.getDeviceId();
        device_name = Build.MODEL;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version_name = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        textLabelLoginSkip = findViewById(R.id.text_label_login_skip);
        textLabelLoginSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_skip = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent_skip);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        text_label_forgot_user_name_password = findViewById(R.id.text_label_forgot_user_name_password);
        text_label_forgot_user_name_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        buttonLogin = findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username_login = editTextLabelLoginUserName.getText().toString().trim();
                String username_paswd = editTextLabelLoginPassword.getText().toString().trim();
                if (username_login.equals("")) {
                    Toast.makeText(LoginActivity.this, "Please enter your username/email", Toast.LENGTH_SHORT).show();
                } else if (username_paswd.equals("")) {
                    Toast.makeText(LoginActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                } else if (username_paswd.length() <= 8) {
                    Toast.makeText(LoginActivity.this, "Please enter your correct password", Toast.LENGTH_SHORT).show();
                } else {

                    getData();
                }

            }
        });
        buttonLoginFacebook = findViewById(R.id.button_login_facebook);

        buttonLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog();
            }
        });
        text_label_sign_up = findViewById(R.id.text_label_sign_up);
        text_label_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            }
        });

    }

   /* @OnClick({R.id.button_login,
            R.id.button_login_facebook, R.id.button_login_gmail, R.id.text_label_sign_up})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.button_login:
                 *//* Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                  startActivity(intent);
                  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);*//*

                break;
            case R.id.button_login_facebook:
                break;
            case R.id.button_login_gmail:
                break;
            case R.id.text_label_sign_up:
                Intent intent1 = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
        }*/
    //   }


    public void Dialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.threwnttousefa);
        alertDialogBuilder.setPositiveButton((getString(R.string.continu)),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("text/plain");
                        shareAppLinkViaFacebook("https://www.facebook.com");
                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        share.putExtra(Intent.EXTRA_SUBJECT, "Cinima Time");
                        share.putExtra(Intent.EXTRA_TEXT, "Invite your friends to CinemaTime via http://www.dotcominfoway.com");
                        startActivity(Intent.createChooser(share, "Invite Via"));
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                //finish();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void shareAppLinkViaFacebook(String urlToShare) {
        try {
            Intent intent1 = new Intent();
            intent1.setClassName("com.facebook.katana", "com.facebook.katana.activity.composer.ImplicitShareIntentHandler");
            intent1.setAction("android.intent.action.SEND");
            intent1.setType("text/plain");
            intent1.putExtra("android.intent.extra.TEXT", urlToShare);
            startActivity(intent1);
        } catch (Exception e) {
            // If we failed (not native FB app installed), try share through SEND
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
            startActivity(intent);

        }
    }

    private void getData() {

        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait.....");
            username_login = editTextLabelLoginUserName.getText().toString();
            username_paswd = editTextLabelLoginPassword.getText().toString();

             cinemaTimeAPI.login(username_login, username_paswd, Settings.Secure.ANDROID_ID, device_name, device_id, Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),version_name).enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    hideProgress();

                    if (response.body() != null) {
                        LoginModel model = response.body();
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(model);
                        Log.e("login response", "" + json);
                        if (model.getStatus().equals("Success")) {
                            editor.putInt(Constants.LOGINSTATUS, 1);
                            editor.putInt(Constants.Userid,model.getResults().getId());
                            editor.putString(Constants.USERNAME,model.getResults().getUser_fname());
                            editor.putString(Constants.MOBILENUMBER,model.getResults().getPhone());
                            editor.commit();
                            AppPreference.setLoggedIn(LoginActivity.this, true);
                            Intent intent_new = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent_new);
                            // UtilsDefault.updateSharedPreference(Constants.Userid,String.valueOf(model.getMovieDetailsReviewResults().getId()));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            Toast.makeText(LoginActivity.this, "success", Toast.LENGTH_SHORT).show();
                        }else{
                           // hideProgress();
                            Toast.makeText(LoginActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(LoginActivity.this, "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            hideProgress();
            Toast.makeText(this, "no internet", Toast.LENGTH_SHORT).show();

        }

    }
}

