package com.dci.cinematime.activity;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.dci.cinematime.Adapter.Viewpager_adapter;
import com.dci.cinematime.R;
import com.dci.cinematime.fragment.ComingSoonFragment;

import com.dci.cinematime.fragment.FragmentHome;
import com.dci.cinematime.fragment.NowShowingFragment;
import com.dci.cinematime.fragment.SearchFragment;
import com.dci.cinematime.fragment.SettingFragment;
import com.dci.cinematime.fragment.TheatresFragment;

public class HomeActivity extends BaseActivity implements AHBottomNavigation.OnTabSelectedListener {


    AHBottomNavigation bottomNavigationView;
    android.support.v4.app.Fragment fragment;
    ConstraintLayout constrain_home_tool_bar;
  public   ImageView image_filter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        image_filter = findViewById(R.id.image_filter);
        bottomNavigationView = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnTabSelectedListener(this);
        bottomNavigationView.setAccentColor(Color.parseColor("#DA3D31"));
       // bottomNavigationView.setBackground(getDrawable(R.drawable.background_notifycolour));
        createitems();
        constrain_home_tool_bar = findViewById(R.id.constrain_home_tool_bar);
        fragment = new FragmentHome();
        setFragment(fragment);

    }



   /* private void showBottomSheetDialog() {

        View view = getLayoutInflater().inflate(R.layout.nowshowing_bottom_sheet_dialog_filter, null);
        NowshowingBottomSheetFragment nowshowingBottomSheetFragment=new NowshowingBottomSheetFragment();
        nowshowingBottomSheetFragment.show(getSupportFragmentManager(),nowshowingBottomSheetFragment.getTag());


    }
*/
    public void hidetoolbar() {
        constrain_home_tool_bar.setVisibility(View.GONE);
    }

    public void showtoolbar() {
        constrain_home_tool_bar.setVisibility(View.VISIBLE);


    }


    private void createitems() {


        AHBottomNavigationItem homeitem = new AHBottomNavigationItem(getString(R.string.home), R.mipmap.show_type_iconthreex);
        AHBottomNavigationItem theatreitem = new AHBottomNavigationItem(getString(R.string.theatres), R.mipmap.theatre_iconthreex);
        AHBottomNavigationItem searchitem = new AHBottomNavigationItem(getString(R.string.search), R.mipmap.search_iconthreex);
        AHBottomNavigationItem settingitem = new AHBottomNavigationItem(getString(R.string.settings), R.mipmap.setings_iconthreex);

        //      AHBottomNavigationItem dashItem=new AHBottomNavigationItem(getString(R.string.dashboard),R.drawable.dashboard_icon);
        //     AHBottomNavigationItem contactItem=new
        bottomNavigationView.addItem(homeitem);
        bottomNavigationView.addItem(theatreitem);
        bottomNavigationView.addItem(searchitem);
        bottomNavigationView.addItem(settingitem);
        bottomNavigationView.setCurrentItem(0);


    }


    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {

        if (position == 0) {
            fragment = new FragmentHome();
            setFragment(fragment);
        } else if (position == 1) {
            fragment = new TheatresFragment();
            setFragment(fragment);
        } else if (position == 2) {
            fragment = new SearchFragment();
            setFragment(fragment);
        } else if (position == 3) {
            fragment = new SettingFragment();
            setFragment(fragment);


        }
        return true;
    }


    public void setFragment(android.support.v4.app.Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.constraint_tap_layout_home, fragment, "About Us");
            fragmentTransaction.commit();
        }
    }

}
