package com.dci.cinematime.activity;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.dci.cinematime.Adapter.Customadapter_home_filter_popup;

import com.dci.cinematime.R;

import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.HomeFilterPopup_cl;

import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class HomeFilterPopupActivity extends BaseActivity {

    private List<HomeFilterPopup_cl> filter = new ArrayList<>();
    private List<String> language;
    private RecyclerView recyclerView;
    boolean isrefresh;
    String value,lan;
    int postion;
    ProgressDialog progressDialog;
    private Customadapter_home_filter_popup mAdapter;
    TextView text_label_popularity, text_label_release_date, text_label_albhabetically, text_label_reset_nowshowing_bottom;
    ImageView image_now_showing_filter_tick, image_now_showing_filter_tick_second, image_now_showing_filter_tick_three, image_now_showing_filter_back_arrow;
    Button button_filter_one_cancel, button_filter_two_apply;
    public static int sortby=1;
    public static String selectedlanguage=" ";
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_filter_popup);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        image_now_showing_filter_back_arrow = findViewById(R.id.image_now_showing_filter_back_arrow);
        button_filter_one_cancel = findViewById(R.id.button_filter_one_cancel);
        button_filter_two_apply = findViewById(R.id.button_filter_two_apply);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_now_showing_filter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        popup_language();

        image_now_showing_filter_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortby=1;
                selectedlanguage="";
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
        button_filter_one_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
        button_filter_two_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(HomeFilterPopupActivity.this, NowShowingFragment.class);
                intent.putExtra("nowshowing",1);

                startActivity(intent);*/

                Fragment fragment = new Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("nowshowing",value);
                bundle.putString("nowshowinglan",lan);
                fragment.setArguments(bundle);

                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);


            }
        });
        text_label_reset_nowshowing_bottom = findViewById(R.id.text_label_reset_nowshowing_bottom);
        text_label_reset_nowshowing_bottom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                View view = recyclerView.getChildAt(postion);
                ImageView imgTick = view.findViewById(R.id.image_now_showing_filter_tick_four);
                imgTick.setVisibility(View.GONE);
                sortby=1;
                selectedlanguage="";
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);

               // mAdapter.notifyDataSetChanged();

            }
        });
        text_label_popularity = findViewById(R.id.text_label_popularity);
        image_now_showing_filter_tick = findViewById(R.id.image_now_showing_filter_tick);
        image_now_showing_filter_tick_second = findViewById(R.id.image_now_showing_filter_tick_second);
        image_now_showing_filter_tick_three = findViewById(R.id.image_now_showing_filter_tick_three);

        text_label_popularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="1";
                sortby=1;
            }
        });
        text_label_release_date = findViewById(R.id.text_label_release_date);
        text_label_release_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="2";
                sortby=2;
            }
        });
        text_label_albhabetically = findViewById(R.id.text_label_albhabetically);
        text_label_albhabetically.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.VISIBLE);
                value="3";
                sortby=3;

            }
        });


    }

    private void popup_language() {
        if (Util.isNetworkAvailable()) {
           /* if (!isrefresh){
                progressDialog.setMessage("Please Wait......");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }*/
           showProgress();

            cinemaTimeAPI.getLang().enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    try {
                        language = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = object.getJSONArray("MovieDetailsReviewResults");

// get category JSONObject from mainJSONObj
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject object1 = jsonArray.getJSONObject(i);
                            Iterator<String> iterator = object1.keys();
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                Log.i("TAG", "key:" + key + "--Value::" + object1.optString(key));
                                HomeFilterPopup_cl cl = new HomeFilterPopup_cl();
                                cl.setText_label_english_now_showing_filter(object1.optString(key));
                                filter.add(cl);
                                language.add(key);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

// get all keys from categoryJSONObj

                    if (response.body() != null) {
                        // String model_lang = response.body();

                        if (filter.size() != 0) {
                            mAdapter = new Customadapter_home_filter_popup(filter, getApplicationContext());
                            recyclerView.setAdapter(mAdapter);
                            mAdapter.setOnClickListen(new Customadapter_home_filter_popup.AddTouchListen() {
                                @Override
                                public void onTouchClick(int position) {
                                    postion=position;
                                    Log.d(TAG, "onTouchClick: " + position);

                                    selectedlanguage=language.get(position);
                                        Log.e("filter", "language" + language.get(position));
                                        //startActivity(new Intent(HomeFilterPopupActivity.this, TheatreDetailsActivity.class));

                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(HomeFilterPopupActivity.this, "failure", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            hideProgress();
            Toast.makeText(HomeFilterPopupActivity.this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
