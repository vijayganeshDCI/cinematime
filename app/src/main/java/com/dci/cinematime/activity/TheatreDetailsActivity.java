package com.dci.cinematime.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.Customadapter_theatre_details;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.TheatreDetails_cl;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TheatreDetailsActivity extends BaseActivity {

    HorizontalCalendar horizontalCalendar;

    private List<TheatreDetails_cl> theatre = new ArrayList<>();
    private RecyclerView recyclerView;
    private Customadapter_theatre_details mAdapter;
    ImageView image_theatre_settings_back_arrow,image_theatre_settings_filter_icon;
    String selectedDateStr;
    TextView text_label_theatre_setting_heading_date_years;
    int theatreId;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    String formattedDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theatre_details);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        text_label_theatre_setting_heading_date_years=findViewById(R.id.text_label_theatre_setting_heading_date_years);
        image_theatre_settings_filter_icon=findViewById(R.id.image_theatre_settings_filter_icon);
        image_theatre_settings_filter_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(TheatreDetailsActivity.this,HomeFilterPopupActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);

        recyclerView = (RecyclerView) findViewById(R.id.recycle_theatre_details);
        image_theatre_settings_back_arrow = findViewById(R.id.image_theatre_settings_back_arrow);
        mAdapter = new Customadapter_theatre_details(theatre, TheatreDetailsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            theatreId = bundle.getIntExtra("theatreid", 0);

        }
        theatredetails();
        selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", Calendar.getInstance()).toString();
        text_label_theatre_setting_heading_date_years.setText(selectedDateStr);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 1);


        /* end after 2 months from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        // Default Date set to Today.
        final Calendar defaultSelectedDate = Calendar.getInstance();
       // horizontalCalendar.getSelectedDatePosition()

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.horizondalcalendarview)
                .range(Calendar.getInstance(), endDate)
                .datesNumberOnScreen(5)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.LTGRAY, Color.WHITE)
                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffd54f"))
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                .addEvents(new CalendarEventsPredicate() {

                    Random rnd = new Random();

                    @Override
                    public List<CalendarEvent> events(Calendar date) {
                        List<CalendarEvent> events = new ArrayList<>();
                        //int count = rnd.nextInt(6);

                       /* for (int i = 0; i <= count; i++){
                            events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                        }*/

                        return events;
                    }
                })
                .build();

        Log.i("Default Date", DateFormat.format("EEE, MMM d, yyyy", defaultSelectedDate).toString());

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                text_label_theatre_setting_heading_date_years.setText(selectedDateStr);
                //Toast.makeText(TheatreDetailsActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);
            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
                super.onCalendarScroll(calendarView, dx, dy);
                //Toast.makeText(TheatreDetailsActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onDateLongClicked(Calendar date, int position) {
                Toast.makeText(TheatreDetailsActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                return super.onDateLongClicked(date, position);

            }
        });


        image_theatre_settings_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });

    }

    private void theatredetails() {

        if (Util.isNetworkAvailable())
        {
            showProgress();
            cinemaTimeAPI.getTheatreMovieList(theatreId,formattedDate).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    if (response.body()!=null) {

                    }
                    else
                    {
                        
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                }
            });
        }
        else
        {

        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
