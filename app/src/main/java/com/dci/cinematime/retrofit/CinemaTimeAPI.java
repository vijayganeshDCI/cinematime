package com.dci.cinematime.retrofit;


import com.dci.cinematime.model.ComingSoon_cl;
import com.dci.cinematime.model.Exclusive_cl;
import com.dci.cinematime.model.LoginModel;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailsResponse;
import com.dci.cinematime.model.MovieDetailsReviewResponse;
import com.dci.cinematime.model.NotifyMe_cl;
import com.dci.cinematime.model.NowShowing_cl;
import com.dci.cinematime.model.SearchResponse;
import com.dci.cinematime.model.SignupModel;
import com.dci.cinematime.model.TheatersList;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface CinemaTimeAPI {



    @FormUrlEncoded
    @POST("api/registration")
    Call<SignupModel> signup(@Field("user_fname") String user_fname,
                             @Field("email") String email,
                             @Field("phone") String phone,
                             @Field("password") String password,
                             @Field("device_id") String device_id,
                             @Field("device_name") String device_name,
                             @Field("device_imei") String device_imei,
                             @Field("device_os") String device_os,
                             @Field("app_version") String app_version);



    @FormUrlEncoded
    @POST("api/login")
    Call<LoginModel> login(@Field("email") String email,
                           @Field("password") String password,
                           @Field("device_id") String device_id,
                           @Field("device_name") String device_name,
                           @Field("device_imei") String device_imei,
                           @Field("device_os") String device_os,
                           @Field("app_version") String app_version);



    @FormUrlEncoded
    @POST("api/comingsoon")
    Call<ComingSoon_cl> coming(@Field("user_id") String user_id,
                               //@Field("setuser_id") String setuser_id,
                               @Field("sortBy") String sortBy,
                               @Field("language") String language);



    @FormUrlEncoded
    @POST("api/nowShowing")
    Call<NowShowing_cl> nowshow(@Field("sortBy") String sortBy,
                                @Field("language") String language);



    @FormUrlEncoded
    @POST("api/exclusive")
//remaining url
    Call<Exclusive_cl> execlu(@Field("sortBy") String sortBy,
                              @Field("language") String language);



    @GET("api/movieLanguages")
    Call<JsonElement> getLang();



    @GET("api/theatres")
    Call<TheatersList> getTheaterList();


    @FormUrlEncoded
    @POST("api/notifyMe")
    Call<NotifyMe_cl>noti(@Field("movie_id")int movie_id,
                          @Field("user_id")int user_id);


    @FormUrlEncoded
    @POST("api/search")
    Call<JsonElement>getmoviesearch(@Field("keyword")String keyword);



    @FormUrlEncoded
    @POST("api/movieDetails")
    Call<JsonElement>getmoviesDetail(@Field("movie_id")int movie_id);

    @FormUrlEncoded
    @POST("api/movieReviews")
    Call<MovieDetailsReviewResponse>getMovieReview(@Field("movie_id")int movie_id,
                          @Field("page")int page);


    @FormUrlEncoded
    @POST("api/addReview")
    Call<JsonElement>addmovieReview(@Field("movie_id")int movie_id, @Field("user_id")int user_id,@Field("reviews")String reviews);



    @FormUrlEncoded
    @POST("api/movieInTheatres")
    Call<JsonElement>getMovieThreate(@Field("movie_id")int movie_id, @Field("date")String date,@Field("time")String time);

    @FormUrlEncoded
    @POST("api/showTimes")
    Call<JsonElement>getMovieShowtime(@Field("movie_id")int movie_id);


    @FormUrlEncoded
    @POST("api/theatreMovies")
    Call<JsonElement>getTheatreMovieList(@Field("theatre_id")int theatre_id,
                          @Field("date")String date);

}





