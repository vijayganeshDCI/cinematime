package com.dci.cinematime.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dci.cinematime.Adapter.Viewpager_adapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment {

    ViewPager viewPager1;
    TabLayout tabLayout;
    SmartTabLayout smartTabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    changeTabsFont();
        View view = inflater.inflate(R.layout.fragment_fragment_home, container, false);
        viewPager1 = (ViewPager) view.findViewById(R.id.viewpager_home);
        setupViewpager(viewPager1);
        smartTabLayout=(SmartTabLayout) view.findViewById(R.id.tab_home);
        smartTabLayout.setViewPager(viewPager1);
       // smartTabLayout.setupWithViewPager(viewPager1);


        /*tabLayout = (tabLayout) view.findViewById(R.id.tab_home);
        tabLayout.setupWithViewPager(viewPager1);*/
       // tabLayout.setBackground(getContext().getDrawable(R.drawable.background_notifycolour));
        viewPager1.setOffscreenPageLimit(3);

        ((HomeActivity)getActivity()).showtoolbar();//this calling method is show the tool bar

        return view;
    }

    void setupViewpager(ViewPager viewPager) {
        Viewpager_adapter adapter = new
                Viewpager_adapter(getChildFragmentManager());
        adapter.addFragment(new NowShowingFragment(), "Now Showing");
        adapter.addFragment(new ComingSoonFragment(), "Coming Soon");
        adapter.addFragment(new ExclusiveFragment(), "Exclusive");

        viewPager.setAdapter(adapter);

    }
}
  /*  private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Font.getInstance().getTypeFace(), Typeface.NORMAL);
                }
            }
        }
    }

}*/
