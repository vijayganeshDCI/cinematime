package com.dci.cinematime.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.MovieReviewAdapter;
import com.dci.cinematime.Adapter.MovieTheaterAdapter;
import com.dci.cinematime.Adapter.MovieTimeListAdpater;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailReviewItem;
import com.dci.cinematime.model.MovieDetailsTheatresList;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.github.jhonnyx2012.horizontalpicker.DatePickerListener;
import com.github.jhonnyx2012.horizontalpicker.HorizontalPicker;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 7/25/2018.
 */

public class ShowtimeFragment extends BaseFragment  implements DatePickerDialog.OnDateSetListener {
    List<MovieDetail> movieDetailList;
    RecyclerView theaterrecycle;
    MovieTheaterAdapter movieTheaterAdapter;
    HorizontalPicker picker;
    LinearLayout dateselect;
    LinearLayout timeselect;
    HorizontalCalendar horizontalCalendar;
    RelativeLayout calendar_view;
    String flag = "0";
    private static int selectedDate = 1;
    private Calendar currentD;
    private Calendar calendardate;
    private SimpleDateFormat simpleDateFormat, simpleDateFormat2;
    private Calendar calendarTime;
    private Calendar currentT;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public ArrayList<String> showtimeList;
    Spinner showtime_spinner;
    MovieTimeListAdpater timeAdapter;
    String formattedDate;
    List<MovieDetailsTheatresList> movieDetailTheatresList;
    List<ShowTimeMovieTiminginTheater> movieTimingDetailTheatres;
    ArrayList<String> movietimeinTheatres;
    boolean apiservice=false;
    TextView selecteddate;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    changeTabsFont();
        View view = inflater.inflate(R.layout.fragment_showtime, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        picker = (HorizontalPicker) view.findViewById(R.id.datePicker);
        theaterrecycle = view.findViewById(R.id.recycle);
        dateselect = view.findViewById(R.id.date_select);
        timeselect = (LinearLayout) view.findViewById(R.id.time_select);
        calendar_view = view.findViewById(R.id.calendar_view);
        showtime_spinner=(Spinner)view.findViewById(R.id.spinner_showtime);
        selecteddate=(TextView)view.findViewById(R.id.selected_date);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
        movieDetailTheatresList=new ArrayList<MovieDetailsTheatresList>();
        movietimeinTheatres=new ArrayList<String>();
        showtimeList=new ArrayList<String>();
        movieTimingDetailTheatres=new ArrayList<ShowTimeMovieTiminginTheater>();
        dateselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog();
            }
        });
        timeselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //timePickerDialog();
            }
        });
        timeAdapter = new MovieTimeListAdpater(showtimeList,getContext());
        showtime_spinner.setAdapter(timeAdapter);

        movieDetailList = new ArrayList<>();

        movieTheaterAdapter = new MovieTheaterAdapter(getActivity(), movieDetailTheatresList);
        theaterrecycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        theaterrecycle.setItemAnimator(new DefaultItemAnimator());
        theaterrecycle.setAdapter(movieTheaterAdapter);
        showtime_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getMovieThreatelist(formattedDate,showtimeList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    private void datePickerDialog() {
        if (selectedDate == 1) currentD = Calendar.getInstance();
        else currentD = calendardate;
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance((DatePickerDialog.OnDateSetListener) this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        datePickerDialog.setMinDate(currentD);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }




    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String donateDate=simpleDateFormat.format(calendardate.getTime());
        Date dob = null;
        try {
            dob = simpleDateFormat.parse(donateDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formattedDate= simpleDateFormat.format(dob);
        selecteddate.setText(formattedDate);


    }



    private void getMovieThreatelist(String date,String time) {
        if (Util.isNetworkAvailable()) {
            movieDetailTheatresList.clear();
            movieTimingDetailTheatres.clear();
            showProgress();

             cinemaTimeAPI.getMovieThreate(MovieDetailActivity.movieId,date,time).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    if (response.body()!=null) {
                        JSONObject object = null;
                        try {
                            object = new JSONObject(String.valueOf(response.body()));
                            JSONObject ResultsjsonArray = object.getJSONObject("Results");
                            JSONArray theatresjsonArray = ResultsjsonArray.getJSONArray("theatres");
                            for (int i = 0; i < theatresjsonArray.length(); i++) {
                                JSONObject theatresjsonArrayObject = theatresjsonArray.getJSONObject(i);
                                JSONArray showtimesjsonArray = theatresjsonArrayObject.getJSONArray("showtimes");
                                for (int k = 0; k < showtimesjsonArray.length(); k++) {
                                    JSONObject showtimesjsonjsonArrayObject = showtimesjsonArray.getJSONObject(k);
                                    //movietimeinTheatres.add(showtimesjsonjsonArrayObject.getString("time"));
                                    movieTimingDetailTheatres.add(new ShowTimeMovieTiminginTheater(showtimesjsonjsonArrayObject.getString("time")));

                                }
                                movieDetailTheatresList.add(new MovieDetailsTheatresList(theatresjsonArrayObject.getString("name"),
                                        theatresjsonArrayObject.getString("Latitude"),theatresjsonArrayObject.getString("Longitude"),
                                        theatresjsonArrayObject.getString("Email"),theatresjsonArrayObject.getString("NoofScreens")
                                        , (ArrayList<ShowTimeMovieTiminginTheater>) movieTimingDetailTheatres));

                                }


                            movieTheaterAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                }
            });

        }
        else
        {
            Toast.makeText(getContext(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMovieShowtime()
    {
        if (Util.isNetworkAvailable()) {

             showProgress();

            cinemaTimeAPI.getMovieShowtime(MovieDetailActivity.movieId).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hideProgress();
                if (response.body()!=null)
                {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = object.getJSONArray("Results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject showtimeObject = jsonArray.getJSONObject(i);
                            showtimeList.add(showtimeObject.getString("showtimes"));


                        }
                        timeAdapter.notifyDataSetChanged();
                        //getMovieThreatelist(formattedDate,showtimeList.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {

                }
            });

        }
        else
        {
            Toast.makeText(getContext(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {

            //getMovieThreatelist(formattedDate,currentTime);
            getMovieShowtime();


        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;

            //getMovieThreatelist(formattedDate,currentTime);
            getMovieShowtime();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
}
