package com.dci.cinematime.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.Customadapter_coming_soon;
import com.dci.cinematime.Adapter.Customadapter_now_showing;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.activity.SignupActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.ComingSoon_cl;
import com.dci.cinematime.model.NotifyMe_cl;
import com.dci.cinematime.model.SignupModel;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.dci.cinematime.utils.UtilsDefault;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.cinematime.utils.ShoppingApplication.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComingSoonFragment extends BaseFragment {

    private List<ComingSoon_cl.Result> coming;
    private RecyclerView recyclerView;
    private Customadapter_coming_soon mAdapter;

    private List<NotifyMe_cl.Results> notify;

    boolean isRefresh;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swip_lay;
    TextView noresult;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    HomeActivity homeActivity;
    public ComingSoonFragment() {
        // Required empty public constructor
    }
    @Inject
    CinemaTimeAPI cinemaTimeAPI;

    BaseActivity baseActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coming_soon, container, false);
        homeActivity=(HomeActivity)getActivity();
        baseActivity = (BaseActivity) getActivity();
        CinemaTimeApplication.getContext().getComponent().inject(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_coming_soon);
        progressDialog = new ProgressDialog(this.getActivity());
        swip_lay = view.findViewById(R.id.swip_lay);
        noresult = view.findViewById(R.id.noresult);
        coming = new ArrayList<>();
        //comingsoon();
        swip_lay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swip_lay.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        coming.clear();
                        getdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);
                        swip_lay.setRefreshing(false);
                    }

                }, 000);
            }
        });
        homeActivity.image_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HomeFilterPopupActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        return view;


    }


    private void getdata(int sortby,String language) {

        if (Util.isNetworkAvailable()) {


            showProgress();
            coming.clear();
             cinemaTimeAPI.coming("0", "1", "").enqueue(new Callback<ComingSoon_cl>() {
                @Override
                public void onResponse(final Call<ComingSoon_cl> call, Response<ComingSoon_cl> response) {
                   /* if (progressDialog!=null){
                        progressDialog.dismiss();
                    }*/

                    if (response.body().getStatus().equals("Failed")) {
                        hideProgress();
                        hiderefresh();
                        Toast.makeText(getActivity(), "No results found", Toast.LENGTH_SHORT).show();
                        noresult.setVisibility(View.VISIBLE);
                    } else {
                        hideProgress();
                        hiderefresh();
                        if (response.body().getResults() != null) {

                            ComingSoon_cl model = response.body();
                            coming.addAll(model.getResults());

                            if (coming.size() != 0) {
                                noresult.setVisibility(View.GONE);
                                mAdapter = new Customadapter_coming_soon(coming, getActivity(),baseActivity);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(mAdapter);

                                mAdapter.setOnClickListen(new Customadapter_coming_soon.AddTouchListen() {

                                    @Override
                                    public void onTouchClick(int position) {
                                        Log.d(TAG, "onTouchClick: " + coming.get(position).getId());
                                        Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                        i.putExtra("id", coming.get(position).getId());
                                        startActivity(i);
                                    }

                                   /* @Override
                                    public void onNotifyTouch(int position) {

                                     CinemaTimeAPI apiService1 = ApiClient.getClient().create(CinemaTimeAPI.class);
                                     Call<NotifyMe_cl> call33 =apiService1.noti("28","22");
                                     call33.enqueue(new Callback<NotifyMe_cl>() {
                                         @Override
                                         public void onResponse(Call<NotifyMe_cl> call, Response<NotifyMe_cl> response) {

                                          //   notify=new ArrayList<>();
                                             if (response.body().getStatus().equals("Failure")){

                                                 hideProgress();
                                                 hiderefresh();
                                                 Toast.makeText(getActivity(), "Don't Notify", Toast.LENGTH_SHORT).show();


                                             }
                                                 hideProgress();
                                                 hiderefresh();
                                                 if (response.body().getMovieDetailsReviewResults()!=null){
                                                     NotifyMe_cl model = response.body();
                                                     Toast.makeText(getActivity(), model.getStatus(), Toast.LENGTH_SHORT).show();

                                                     if (model.getStatus().equals("Success")){

                                                     }
                                                     else {

                                                     }

                                                 }
                                             }


                                         @Override
                                         public void onFailure(Call<NotifyMe_cl> call, Throwable t) {

                                         }
                                     });
                                }*/



                                });
                                swip_lay.setRefreshing(false);
                            } else {
                                Toast.makeText(getActivity(), "no data found", Toast.LENGTH_SHORT).show();
                                swip_lay.setRefreshing(false);
                            }
                        } else {
                            // noresult.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                            swip_lay.setRefreshing(false);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ComingSoon_cl> call, Throwable t) {
                    /*if (progressDialog!=null){
                        progressDialog.dismiss();
                    }*/

                    noresult.setVisibility(View.VISIBLE);
                    hideProgress();
                    hiderefresh();
                    // Toast.makeText(getContext(), "No result found", Toast.LENGTH_SHORT).show();

                    //  Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            /*if (progressDialog!=null){
                progressDialog.dismiss();
            }
            hiderefresh();*/
            hideProgress();
            hiderefresh();
            Toast.makeText(getActivity(), "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            getdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);


        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            getdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


}











   /* public void comingsoon(){
        ComingSoon_cl soo1 = new ComingSoon_cl("Baahubali","Telugu","NOTIFY");

        coming.add(soo1);

        ComingSoon_cl soo2 = new ComingSoon_cl("Baahubali","Telugu","NOTIFY");

        coming.add(soo2);

        ComingSoon_cl soo3 = new ComingSoon_cl("Baahubali","Telugu","NOTIFY");

        coming.add(soo3);

        ComingSoon_cl soo4 = new ComingSoon_cl("Baahubali","Telugu","NOTIFY");

        coming.add(soo4);

        ComingSoon_cl soo5 = new ComingSoon_cl("Baahubali","Telugu","NOTIFY");

        coming.add(soo5);
    }
*/

