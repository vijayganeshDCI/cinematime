package com.dci.cinematime.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.Customadapter_now_showing;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.NowShowing_cl;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.cinematime.utils.ShoppingApplication.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class NowShowingFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private List<NowShowing_cl.Result> showing;
    private RecyclerView recyclerView;
    private Customadapter_now_showing mAdapter;
    boolean isRefresh = false;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swip_lay;
    HomeActivity homeActivity;

    LinearLayoutManager linearLayoutManager;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public NowShowingFragment() {
        // Required empty public constructor
    }

    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sample, container, false);
        homeActivity=(HomeActivity)getActivity();
        CinemaTimeApplication.getContext().getComponent().inject(this);
//        homeFilterPopupActivity=(HomeFilterPopupActivity)getActivity();
        recyclerView = view.findViewById(R.id.recycle_now_showing);
        swip_lay = view.findViewById(R.id.swip_lay);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayoutManager);
        showing = new ArrayList<>();
        mAdapter = new Customadapter_now_showing(showing, getActivity());
        recyclerView.setAdapter(mAdapter);
        //nowshowing();

        //getdata(1,"");
        homeActivity.image_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HomeFilterPopupActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        swip_lay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swip_lay.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                            showing.clear();
                        getNowshowingdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);
                            swip_lay.setRefreshing(false);
                        }

                }, 000);
            }
        });

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
       /* swip_lay.post(new Runnable() {
                          @Override
                          public void run() {
                              swip_lay.setRefreshing(true);

                              getdata();
                          }
                      }
        );*/

        return view;


    }

    @Override
    public void onResume() {
        super.onResume();

       // getdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);



    }

    private void getNowshowingdata(int sortby,String language) {
        if (Util.isNetworkAvailable()) {
          //  if (isRefresh) {

                showProgress();
           // }
          //  swip_lay.setRefreshing(true);

             cinemaTimeAPI.nowshow(String.valueOf(sortby), language).enqueue(new Callback<NowShowing_cl>() {
                @Override
                public void onResponse(Call<NowShowing_cl> call, Response<NowShowing_cl> response) {
                    hideProgress();
                    hiderefresh();

                    showing.clear();
                    if (response.body() != null) {

                        NowShowing_cl model = response.body();
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(model);
                        Log.e("", "nowshowing: " + json);

                        showing.addAll(model.getResults());

                        if (showing.size() != 0) {


                                mAdapter.notifyDataSetChanged();

                            mAdapter.setOnClickListen(new Customadapter_now_showing.AddTouchListen() {

                                @Override
                                public void onTouchClick(int position) {
                                    Log.d(TAG, "onTouchClick: " + showing.get(position).getId());
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);

                                    i.putExtra("id", showing.get(position).getId());
                                    startActivity(i);
                                }
                            });
                            swip_lay.setRefreshing(false);
                        } else {
                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                            swip_lay.setRefreshing(false);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Server error", Toast.LENGTH_SHORT).show();
                        swip_lay.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<NowShowing_cl> call, Throwable t) {
                    hiderefresh();
                    hideProgress();
                    swip_lay.setRefreshing(false);
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }



   /* private void nowshowing(){
        NowShowing_cl nw1 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw1);

        NowShowing_cl nw2 = new NowShowing_cl("Ocean 14","English-Arabic","U/A","95%","98,000 views");

        showing.add(nw2);

        NowShowing_cl nw3 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw3);

        NowShowing_cl nw4 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw4);

        NowShowing_cl nw5 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw5);
    }*/
   @Override
   public void onStart() {
       super.onStart();
       isStarted = true;
       if (isVisible) {
           getNowshowingdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);


       }


   }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            getNowshowingdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }



}
