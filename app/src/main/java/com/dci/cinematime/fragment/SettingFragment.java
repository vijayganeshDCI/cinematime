package com.dci.cinematime.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    ConstraintLayout constraint_inside_three;


    public SettingFragment() {
        // Required empty public constructor
    }
    TextView text_label_logout_setting_fragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        constraint_inside_three=view.findViewById(R.id.constraint_inside_three);
        constraint_inside_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_logout = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent_logout);
                getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                getActivity().finish();
            }
        });
        text_label_logout_setting_fragment=view.findViewById(R.id.text_label_logout_setting_fragment);

        ((HomeActivity)getActivity()).hidetoolbar();//this calling method is hide the tool
        return view;

    }
}
