package com.dci.cinematime.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.cinematime.Adapter.Customadapter_coming_soon;
import com.dci.cinematime.Adapter.Customadapter_exclusive;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.ComingSoon_cl;
import com.dci.cinematime.model.Exclusive_cl;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExclusiveFragment extends BaseFragment {

    private List<Exclusive_cl.Results.Datum> exclu = new ArrayList<>();
    private RecyclerView recyclerView;
    private Customadapter_exclusive mAdapter;
    boolean isRefresh = false;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swip_lay;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public ExclusiveFragment() {
        // Required empty public constructor
    }
    @Inject
    CinemaTimeAPI cinemaTimeAPI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exclusive, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_exclusive);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
       /* mAdapter = new Customadapter_exclusive(exclu, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);*/
        swip_lay = view.findViewById(R.id.swip_lay);
         //exclusive();
        swip_lay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swip_lay.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        exclusive();
                        swip_lay.setRefreshing(false);
                    }

                }, 000);
            }
        });

        return view;
    }

    public void exclusive() {
        if (Util.isNetworkAvailable()) {
            exclu.clear();
            showProgress();

           cinemaTimeAPI.execlu("1", "").enqueue(new Callback<Exclusive_cl>() {
                @Override
                public void onResponse(retrofit2.Call<Exclusive_cl> call, Response<Exclusive_cl> response) {
                    hideProgress();
                    if (response.body() != null) {
                        List<Exclusive_cl.Results.Datum> model_exe = response.body().getResults().getData();

                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(model_exe);
                        Log.e("login response", "" + json);
                        exclu.addAll(model_exe);
                        if (model_exe != null) {
                            mAdapter = new Customadapter_exclusive(model_exe, getActivity());
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            Toast.makeText(getActivity(), "no data found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "api error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<Exclusive_cl> call, Throwable t) {
                    hideProgress();
//                    Toast.makeText(getActivity(), "exclusive", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getActivity(), "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
          exclusive();


        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
           exclusive();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
}

