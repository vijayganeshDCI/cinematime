package com.dci.cinematime.fragment;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.cinematime.Adapter.TheatersAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.ShoppingApplication;
import com.dci.cinematime.utils.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class TheatresFragment extends BaseFragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final char MY_LOCATION_REQUEST_CODE = 0;
    private GoogleMap mMap;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    RecyclerView recyclerView;
    TheatersAdapter theatersAdapter;
    List<TheatersList.ResultsItem> theaterList;
    LocationRequest mLocationRequest;
    boolean isRefresh = false;
    private TheatresFragment mapFrag;
    LinearLayoutManager linearLayoutManager;

    public void Oncreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public TheatresFragment() {
        // Required empty public constructor
    }
    @Inject
    CinemaTimeAPI cinemaTimeAPI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_theatres, container, false);
        ((HomeActivity) getActivity()).hidetoolbar();//this calling method is show the tool
        CinemaTimeApplication.getContext().getComponent().inject(this);
        recyclerView = view.findViewById(R.id.recycle_theatre_fragment);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayoutManager);
        getdata();
        showProgress();
/*

        theatersAdapter = new TheatersAdapter(theaterList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(theatersAdapter);
*/

/*
        theatersAdapter.setOnClickListen(new TheatersAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.d(TAG, "onTouchClick: "+position);
                startActivity(new Intent(getActivity(), TheatreDetailsActivity.class));

            }
        });*/
        return view;

    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//        try {
//            if (ActivityCompat.checkSelfPermission(getActivity(),
//                    Manifest.permission.ACCESS_FINE_LOCATION)
//                    == PackageManager.PERMISSION_GRANTED) {
//       /* if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }*/
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("", "" + e);
//        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
//        mLastLocation = location;
//        if (mCurrLocationMarker != null) {
//            mCurrLocationMarker.remove();
//        }
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//        mCurrLocationMarker = mMap.addMarker(markerOptions);
//
//        //move map camera
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
            } else {
                // Permission was denied. Display an error message.
            }
        }
    }

    private void getdata() {
        if (Util.isNetworkAvailable()) {

            //  swip_lay.setRefreshing(true);

            cinemaTimeAPI.getTheaterList().enqueue(new Callback<TheatersList>() {
                @Override
                public void onResponse(Call<TheatersList> call, Response<TheatersList> response) {

                    theaterList = new ArrayList<>();
                    theaterList.clear();
                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(response.body());
                    Log.e("list", "theater: " + json);
                    if (response.body() != null) {

                        TheatersList model = response.body();

                        Log.e("", "theater: " + json);

                        theaterList.addAll(model.getResults());

                        if (theaterList.size() != 0) {
                            Log.e(TAG, "onResponse: " + theaterList.size());

                            theatersAdapter = new TheatersAdapter(theaterList, getActivity());
                            recyclerView.setAdapter(theatersAdapter);
                                setMapdata();
                            theatersAdapter.setOnClickListen(new TheatersAdapter.AddTouchListen() {

                                @Override
                                public void onTouchClick(int position) {
                                    Log.d(ShoppingApplication.TAG, "onTouchClick: " + theaterList.get(position).getId());
                                    Intent i = new Intent(getActivity(), TheatreDetailsActivity.class);

                                    i.putExtra("theatreid", theaterList.get(position).getId());
                                    startActivity(i);
                                    getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

                                }
                            });
                            // swip_lay.setRefreshing(false);
                        } else {
                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                            //swip_lay.setRefreshing(false);
                        }
                        hideProgress();
                      //  hiderefresh();
                    } else {
                        Toast.makeText(getActivity(), "Server error", Toast.LENGTH_SHORT).show();
                        hideProgress();
                      //  hiderefresh();
                        // swip_lay.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<TheatersList> call, Throwable t) {
                    hiderefresh();
                    hideProgress();
                    //  swip_lay.setRefreshing(false);
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    private void setMapdata()
    {

        for (int i = 0; i < theaterList.size(); i++) {

            double latitude = Double.parseDouble(theaterList.get(i).getLatitude());
            double longitude = Double.parseDouble(theaterList.get(i).getLongitude());
            MarkerOptions marker = new MarkerOptions();
            marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude)).title(theaterList.get(i).getTheatrename());
            marker.snippet(theaterList.get(i).getAddress());
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.mappin));
            mMap.addMarker(marker);
        }
    }

}

